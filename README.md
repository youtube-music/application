![picture](https://i.ibb.co/XC1T0JY/twmusic-logo.png)

Unofficial Youtube Music application, Open source and multi-platform for all platforms to use.

Enjoy all your Youtube Music steaming just like on the web version all wrapped up into a music application!

&nbsp;&nbsp;&nbsp;&nbsp;

![picture](https://i.ibb.co/gtFhbF2/ytm-screenshot1.png)

![picture](https://i.ibb.co/87pLS9g/ytm-screenshot2.png)

![picture](https://i.ibb.co/JpTZLqV/ytm-screenshot3.png)


&nbsp;&nbsp;&nbsp;&nbsp;

Note: make sure to install playerctl on your distro so the desktop shortcut media controls will work.

&nbsp;&nbsp;&nbsp;&nbsp;

  You can install Youtube from the AUR for Arch/Manjaro distros.
 [Click Here](https://aur.archlinux.org/packages/youtubemusic/)

 ### Download For All platforms (Linux, Mac OS and Windows)
  
  [Click to get the latest release](https://gitlab.com/youtube-music/application/-/releases)

 ### Author
  * Corey Bruce
